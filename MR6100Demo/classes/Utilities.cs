﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

/// <summary>
/// Clase de utilidades diversas para grabar datos a disco o invocaciones a servidor.
/// </summary>
public  class Utilities
    {

    public static string PROXY = "http://proxysis:8080";
    public static string logFile = "log.txt";
    public static string URL_TERMINAL = "http://terminalesdeleste.com/api/"; //nota: la doble barra es intencional
        

    /// <summary>
    /// Get a page from Internet,return string with content.
    /// </summary>
    /// <param name="Url"></param>
    /// <returns></returns>
    public static String getPage(string Url)
        {
        //System.Net.WebException
        try
        {
            WebClient myWebClient = new WebClient();

            if (isThereAPRoxy())
            {
                myWebClient.Proxy = getProxy();
            }
            myWebClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)"); // If you need to simulate a specific browser
            myWebClient.Headers.Add("accept-charset", "windows-1252");

            byte[] myDataBuffer = myWebClient.DownloadData(Url);
            string download = Encoding.Default.GetString(myDataBuffer);
            return download;
        }
        catch (System.Net.WebException we)
        {
            Utilities.LogToDesktop("debug.txt", String.Format("getPage error: {0}", we.Message));
        }
        return "";
        }

    /// <summary>
    /// Used on environments with proxy
    /// </summary>
    /// <returns></returns>
    public static IWebProxy getProxy()
    {
        WebProxy wp = new WebProxy(PROXY);
        wp.UseDefaultCredentials = true;
        return wp;
    }

    /// <summary>
    /// Checks if there is an active proxy.
    /// </summary>
    /// <returns></returns>
    public static bool isThereAPRoxy()
    {
        bool useProxy = !string.Equals(System.Net.WebRequest.DefaultWebProxy.GetProxy(new Uri(PROXY)), PROXY);
        return useProxy;
    }

    /// <summary>
    ///Make a Post to the expected site, return page after invoke. 
    /// </summary>
    /// <param name="msg"></param>
    /// <param name="url_infracciones"></param>
    /// <returns></returns>
    public static string postCall(string msg, string url_infracciones)
    {
        var postData = Encoding.ASCII.GetBytes(msg);
        var request = (HttpWebRequest)WebRequest.Create(url_infracciones);
        if (Utilities.isThereAPRoxy())
        {
           request.Proxy = getProxy();
        }
        request.Method = "POST";
        request.ContentType = "application/x-www-form-urlencoded";
        request.ContentLength = postData.Length;
        using (var stream = request.GetRequestStream())
        {
            stream.Write(postData, 0, postData.Length);
        }
        var response = (HttpWebResponse)request.GetResponse();
        return  new StreamReader(response.GetResponseStream()).ReadToEnd();
    }

    public static string readFile(string filename)
    {
        return  File.ReadAllText(filename);
    }

    public static void writeAllFile(string filename, string datos)
    {
        File.WriteAllText(filename, datos);
    }

        /// <summary>
        /// Writes a line into a filename
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="linea"></param>
        public static void writeLine(string filename, string linea)
        {
            Console.WriteLine(linea);
            using (StreamWriter file = File.AppendText(filename))
            {
                file.WriteLine(linea);
            }
        }

    /// <summary>
    /// input date in the format mm/dd/yyyy hh:mm:ss (IMPORTANT: please use CultureInfo.InvariantCulture, 
    /// for example DateTime.Now.toString(CultureInfo.InvariantCulture)
    /// 
    /// output yyyymmddhhmmss
    /// Improvement: Now I have unit testing, I will take another different approach, more robust
    /// Requirement: separate using a space
    /// Minutes have to be EXACTLY in the format dd:mm, other will fail
    /// invalid values will fail
    /// The problem is that I can refine indefinitely, so better to have an end to this...
    /// </summary>
    /// <param name="fecha"></param>
    /// <returns></returns>
    public static string reversedDate(string fecha)
    {
        string[] parts = fecha.Trim().Split(' ');
        //First part date , second part hour
        if (parts.Length != 2)
        {
            //Error here
        }
        string f = parts[0];
        string h = parts[1];


        string hour_cut = h.Trim().Replace(":", "");
        if (hour_cut.Length!=6) 
        {
            throw new Exception("Fecha invalida: el formato de tiempo es hh:mm");
        }

        string[] date_part = f.Split('/');
        if (date_part.Length != 3)
        {
            //Error here
            throw new Exception("Fecha invalida: tiene que estar en el formato dd/mm/yyyy");
        }
        //IMPORTANT: string.format will IGNORE COMPLETELY FORMAT IF WE DEAL WITH STRINGS, so we need to convert into integers first
        int month;
        if (!int.TryParse(date_part[0], out month))
        {
            //Error here
            throw new Exception("Fecha invalida: mes invalido");
        }

        int day;
        if (!int.TryParse(date_part[1], out day))
        {
            //Error here
            throw new Exception("Fecha invalida: dia invalido");
        }


        string reversed = string.Format("{0:D4}{1:d2}{2:d2}{3}", date_part[2], month, day, hour_cut);
        return reversed;
    }


    /// <summary>
    /// Escribir un archivo de log directamente en el desktop en curso.
    /// </summary>
    /// <param name="linea"></param>
    public static void Log(string linea)
    {
        writeFile(logFile, linea);
    }

    public static void LogToDesktop(string linea)
    {
        LogToDesktop(logFile, linea);
    }


    public static void LogToDesktop(string filename, string linea)
    {
        string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        writeFile(Path.Combine(path, filename), linea);
    }


    /// <summary>
    /// Primer version envia los datos de prueba terminal y codigo y nada mas.
    /// </summary>
    /// <param name="terminal"></param>
    /// <param name="antena"></param>
    /// <param name="tag"></param>
    /// <param name="hora"></param>
    /// <returns></returns>
        public static string assembleUrl(string terminal,string antena,string tag, string hora)
        {
        /*
            string url = string.Format("{0}/terminal/{1}/antena/{2}/tag/{3}/hora/{4}", URL_TERMINAL, terminal, antena,
                tag, hora);
                */
        string url = string.Format("{0}/terminal/{1}/codigo/{2}", URL_TERMINAL, terminal, tag);

        return url;
        }
    /// <summary>
    /// Append line to a file.
    /// </summary>
    /// <param name="filename"></param>
    /// <param name="linea"></param>
    public static void writeFile(string filename, string linea)
    {
        Console.WriteLine(linea);
        using (StreamWriter file = File.AppendText(filename))
        {
            file.WriteLine(string.Format ("{0} - {1}" , DateTime.Now, linea));
        }
    }

        public static string loadFile(string filename)
        {
            if (!File.Exists(filename)) return null;
            return File.ReadAllText((filename));
        }

}
